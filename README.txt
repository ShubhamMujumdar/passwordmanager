Instructions for running Password Manager on localhost:

1. Install WAMPServer from "https://sourceforge.net/projects/wampserver/" with all of its internal components (MySql isn't checked at default-you need to check it yourself). Set default code-editor to the editor of your choice (default is Notepad, which doesn't display all languages of code) and change the default browser as well (options given at installation).

2. Paste this folder (passwordmanager-master) into "C:\wamp64\www"  (for a 64-bit windows device), or into your corresponding "wamp(32/64)\www" folder.

3. Start all of WAMP's Services (Apache, MySql, MariaDB). 

3-commonissue. Incase your Apache Service doesn't run (Wamp logo on taskbar is orange, and typing "localhost" on browser doesn't take you to the WAMP page) it's an issue with the ports. This happens mostly because of apps like "Skype" using up Port 80, which is the default Apache listen port. 

*Changing default Apache port*:

i. Click on the WAMP icon on the taskbar
ii. Hover over Apache and click on httpd.conf
iii. (works only if default code editor has been set at installation) httpd.conf opens up in your code editor. Find '80' and you'll find 3 instances on lines 69,70,71. Replace 80 with 8080 and save the file.
iv. (if default code editor wasn't set, and is still notepad) httpd.conf would open in Notepad. Go to 'save as' and copy the location at which httpd.conf is saved (default: C:\wamp64\bin\apache\apache2.4.41(replace with your version of Apache)\conf) and open httpd.conf with your desired code editor and do the step iii.
v. Now the URL of Password Manager would be localhost:8080/passwordmanager-master. Note that just "localhost" would still not work on the browser, localhost:8080 would.
vi. Continue with steps 4  onwards.


4. Type localhost (localhost:8080, if you followed 3-commonissue) on the browser.

5. The WAMP main page would open, from there go on to phpMyAdmin.

6. login credentials to phpMyAdmin are "username: root, password: (blank), Server Choice: MySQL"

7. Upon logging in, go on to the "import" tab on the navbar. Load the "tablecreate.sql" file located at "C:\wamp64\www\passwordmanager-master" scroll down and click on "Go".

8. Step 7 would create a new database with the name "pwdmanager", with 2 blank tables "pwdlogin" and "pwddata".

9. While you're still on phpMyAdmin check the "Server port number". Default is "Server: MySQL:3308". If its not 3308, go to "link.php" on C:\wamp64\www\passwordmanager-master and change the port number at line 3 to your MySQL port number.

10. Now go to your browser and type "localhost/passwordmanager-master" (or "localhost:8080/passwordmanager-master" if you changed your default Apache port).

11. Password Manager should work.

12. When not in use, click on the WAMP icon on the taskbar and "Stop all Services".


