SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE pwdmanager;

use pwdmanager;

CREATE TABLE `pwddata` (
  `id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `platform` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `pwddata`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pwddata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

CREATE TABLE `pwdlogin` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `sq1` text NOT NULL,
  `sq2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `pwdlogin`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `pwdlogin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

ALTER TABLE `pwddata` ADD FOREIGN KEY (`login_id`) REFERENCES `pwdlogin`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;